from my_twitter_client import *

list_members = get_list_members("TwitterGov", "uk-mps")
list_members.sort(key= lambda x: (x['last_tweet_created'] is not None, x['last_tweet_created']), reverse=True)
user_latest_tweet = get_user_latest_tweet(list_members[0]['screen_name'])

print("*" * 100)
print(f"Latest Tweet by {list_members[0]['screen_name']} at {list_members[0]['last_tweet_created']}\n")
print("Tweet .... ")
print(f"{user_latest_tweet}\n")
print("*" * 100)