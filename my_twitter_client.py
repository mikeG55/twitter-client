import tweepy
from config import config

auth = tweepy.OAuthHandler(config['api_key'], config['api_secret_key'])
auth.set_access_token(config['access_token'], config['access_token_secret'])
api = tweepy.API(auth)


def get_list_members(list_owner, slug_name):
    """
    Gets a list of members for a group

    :param list_owner: str
    :param slug_name: str
    :return: list
    """
    output = []
    for member in tweepy.Cursor(api.list_members, list_owner, slug_name).items():
        try:
            last_tweet = str(member.status.created_at)
        except AttributeError:
            last_tweet = None

        member_dict = {'name': member.name,
                       'screen_name': member.screen_name,
                       'location': member.location,
                       'followers_count': member.followers_count,
                       'statuses_count': member.statuses_count,
                       'last_tweet_created': last_tweet}
        output.append(member_dict)

    return output


def get_user_details(screen_name):
    """
    Get a users details from there screen name

    :param screen_name: str
    :return: dict
    """
    try:
        return api.get_user(screen_name)._json
    except tweepy.error.TweepError as e:
        print(f"Failed to get user details {e}")
        return None


def get_user_latest_tweet(screen_name):
    """
    Get a users last tweet

    :param screen_name:
    :return: str
    """
    try:
        statuses = api.user_timeline(screen_name)
        return statuses[0].text
    except tweepy.error.TweepError as e:
        print(f"Failed to get user latest tweet {e}")
        return None
