import sys
import tweepy
import json
from config import config


def initialize(search_term):
    auth = tweepy.OAuthHandler(config['api_key'], config['api_secret_key'])
    auth.set_access_token(config['access_token'], config['access_token_secret'])
    api = tweepy.API(auth)

    stream_listener = Listener()
    my_stream = tweepy.Stream(auth=api.auth, listener=stream_listener)
    my_stream.filter(track=[search_term], is_async=True)


class Listener(tweepy.StreamListener):

    def on_data(self, data):
        text = json.loads(data)
        print(text)

    def on_error(self, status_code):
        if status_code == 420:
            return False


if __name__ == "__main__":
    arg = sys.argv
    args = arg[1:]
    x = " ".join(args)
    initialize(x)
