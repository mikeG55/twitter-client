config = {
    'api_key': '',
    'api_secret_key': '',
    'request_token_url': 'https://api.twitter.com/oauth/request_token',
    'authorize_url': 'https://api.twitter.com/oauth/authorize',
    'access_token_url': 'https://api.twitter.com/oauth/access_token',
    'access_token': '',
    'access_token_secret': '',

}