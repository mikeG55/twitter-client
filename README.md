# A Tweepy API - Twitter Client
Python 3.6.9 

## Create virtual environment
```bash
cd twitter-client
virtualenv -p python3.6.9 venv
```

## Activate virtual environment
```bash
source venv/bin/activate
```

## Install requirements
```bash
pip install -r requirements.txt
```
Rename **config.live.py** to **config.py**

Add your twitter dev credentials to the config file.
```
config = {
    'api_key': 'your api key',
    'api_secret_key': 'your api secret key',
    'request_token_url': 'https://api.twitter.com/oauth/request_token',
    'authorize_url': 'https://api.twitter.com/oauth/authorize',
    'access_token_url': 'https://api.twitter.com/oauth/access_token',
    'access_token': 'your access token',
    'access_token_secret': 'your access token secret',
}
```

## Examples
```bash
python run_examples.py 
```

To run the twitter streaming client
```bash
python my_twitter_streaming_client.py {add your search terms here}
```